<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
	/**
	 * @Route("/index")
	 */
	public function index()
	{
		return $this->render('index.html.twig', array(
//			'group_id' => '1'
//			'last_username'	=> $lastUsername,
//			'error' 		=> $error,
		));
	}


	/**
	 * @Route("/admin")
	 */
	public function admin()
	{
		return new Response('<html><body>Admin page!</body></html>');
	}
}